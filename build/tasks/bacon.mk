# Copyright (C) 2017 Unlegacy-Android
# Copyright (C) 2017 The LineageOS Project
# Copyright (C) 2018 The PixelExperience Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

ATIGA_TARGET_PACKAGE := $(PRODUCT_OUT)/$(ATIGA_VERSION).zip

.PHONY: bacon atiga
bacon: $(INTERNAL_OTA_PACKAGE_TARGET)
	$(hide) mv $(INTERNAL_OTA_PACKAGE_TARGET) $(ATIGA_TARGET_PACKAGE)
	$(hide) $(MD5SUM) $(ATIGA_TARGET_PACKAGE) | sed "s|$(PRODUCT_OUT)/||" > $(ATIGA_TARGET_PACKAGE).md5sum
	@echo ""
	@echo -e "--------------------------------------------------------------------------------"
	@echo -e "      ▓▓                                                      ░                 "
	@echo -e "      ▓▓▓▓         ▓▓    ▓▓▓▓▓                          ▓▓▓▓▓▓▓▓▓▓   ▒▓▓▓▓▓▓▓▓▓▓"
	@echo -e "     ▓▓▓▓▓▓▓   ▓▓▓▓▓▓▓▓▓▒ ▒ ▓▓    ░▓▓▓  ▓▓▓  ▒▓▓▓▓    ▓▓▓▓    ▓▓▓▓▓▓ ▓▓▓▓▓▓░    "
	@echo -e "    ▓▓▓ ▓▓▓▓▓    ▓▓▓▓     ▓▓▓▓  ▓▓▓  ▒▓▓▓▒  ▓▓  ▓▓▓▓  ▓▓▓▓▓    ▓▓▓▓▓ ▒▓▓▓▓▓▓▓▓▓ "
	@echo -e "   ▓▓▓▓▓▓▓▓▓▓▓   ▓▓▓▓     ▓▓▓▓   ▓▓▓▓ ▓▓     ▓▓▓▓▓▓▓  ▓▓▓▓▓    ▓▓▓▓▓ ▓▒ ▓▓▓▓▓▓▓▓"
	@echo -e "  ▓▓▓    ░▓▓▓▓▓   ▓▓▓▓▒▓▓ ▓▓▓▓  ▓▓▓        ▓▓▓  ▓▓▓▓░  ▓▓▓▓▓▓▓▓▓▓▓   ▓▓▓▓  ▓▓▓▓▓"
	@echo -e " ▓▓▓▓   ▓▓▓▓▓▓▓▓  ░▓▓▓▓▓ ▓▓▓▓▓▓  ▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓ ▓▓▓▒    ▓▓▓▓▓▓       ▓▓▓▓▓▓▓   "
	@echo -e "                                ▓▓▓    ▓▓▓                                      "
	@echo -e "--------------------------------------------------------------------------------"
	@echo -e "Package path : "$(ATIGA_TARGET_PACKAGE)
	@echo -e "Package md5  : `cat $(ATIGA_TARGET_PACKAGE).md5sum | cut -d ' ' -f 1`"
	@echo -e "Package size : `du -h $(ATIGA_TARGET_PACKAGE) | cut -f 1`"
	@echo -e ""

atiga: bacon
