# Set all versions
ifndef ATIGA_BUILD_TYPE
    ATIGA_BUILD_TYPE := COMMUNITY
endif

ATIGA_DATE_YEAR := $(shell date -u +%Y)
ATIGA_DATE_MONTH := $(shell date -u +%m)
ATIGA_DATE_DAY := $(shell date -u +%d)
ATIGA_DATE_HOUR := $(shell date -u +%H)
ATIGA_DATE_MINUTE := $(shell date -u +%M)
ATIGA_BUILD_DATE_UTC := $(shell date -d '$(ATIGA_DATE_YEAR)-$(ATIGA_DATE_MONTH)-$(ATIGA_DATE_DAY) $(ATIGA_DATE_HOUR):$(ATIGA_DATE_MINUTE) UTC' +%s)
ATIGA_BUILD_DATE := $(ATIGA_DATE_YEAR)$(ATIGA_DATE_MONTH)$(ATIGA_DATE_DAY)-$(ATIGA_DATE_HOUR)$(ATIGA_DATE_MINUTE)

ATIGA_PLATFORM_VERSION := 10.0
ATIGA_CODENAME := Ramadhan

TARGET_PRODUCT_SHORT := $(subst atiga_,,$(ATIGA_BUILD))

ATIGA_VERSION_PROP := 1.0
ATIGA_VERSION := AtigaOS_$(ATIGA_BUILD)_$(ATIGA_VERSION_PROP)-$(ATIGA_CODENAME)_$(ATIGA_BUILD_DATE)_$(ATIGA_BUILD_TYPE)

ATIGA_PROPERTIES := \
    id.atiga.version=$(ATIGA_VERSION_PROP) \
    id.atiga.version.code=$(ATIGA_CODENAME) \
    id.atiga.version.display=$(ATIGA_VERSION) \
    id.atiga.build_date=$(ATIGA_BUILD_DATE) \
    id.atiga.build_date_utc=$(ATIGA_BUILD_DATE_UTC) \
    id.atiga.build_type=$(ATIGA_BUILD_TYPE) \
    ro.atiga.maintainer=$(ATIGA_MAINTAINER)
